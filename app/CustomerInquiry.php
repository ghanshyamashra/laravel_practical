<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerInquiry extends Model
{
    protected $table = 'customer_inquiry';

    public function category(){
        return $this->hasOne('App\InquiryCategory','id','category_id');
    }
}
