<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InquiryCategory;
use App\CustomerInquiry;
use App\Http\Requests\InquiryRequest;

class CustomerInquiryController extends Controller
{
    /**
     * @name inquiryListing
     */
    public function inquiryListing(){
        $inquiries = CustomerInquiry::select('id','name','email','image','description','category_id')->paginate(5);
        return view('listing')->with('inquiries',$inquiries);
    }

    /**
     * @name create
     */
    public function create(){
        $inquiryCategory = InquiryCategory::select('id','name')->get();
        return view('create-inquiry')->with('categories',$inquiryCategory);
    }

    /**
     * @name edit
     */
    public function edit($id){
        $inquiry = CustomerInquiry::find($id);
        $inquiryCategory = InquiryCategory::select('id','name')->get();
        $data = [
                'categories' => $inquiryCategory,
                'inquiry' => $inquiry,
        ];
        
        return view('edit-inquiry',$data);
    }

    /**
     * @name save
     */
    public function save(InquiryRequest $request){
        if($request->has('id')){

            $inquiry = CustomerInquiry::find($request->get('id'));
        }else{
            $inquiry = new CustomerInquiry;
        }
        $inquiry->name= $request->get('name');
        $inquiry->email= $request->get('email');
        $inquiry->category_id= $request->get('category');
        $inquiry->description= $request->get('description');
        if($request->hasFile('image')){
            $image = $request->file('image');
            $imageExt = $image->getClientOriginalExtension();
            $fileName = 'inquiry_'. date('d_m_y_h_i_s.').$imageExt;
            $imagePath = storage_path('inquiry');
            if($image->move($imagePath,$fileName)){
                $inquiry->image= $fileName;

            }

        }
        $inquiry->save();
        return redirect()->to('/');
    }

    public function delete($id){
        $inquiry = CustomerInquiry::find($id);
        $inquiry->delete();
        return redirect()->to('/');
    }

}
