<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'category' => 'required|exists:inquiry_category,id',
            'description' => 'required',
            'image' => 'image'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'required',
            'email.required' => 'required',
            'email.email' => 'invalid email',
            'category.required' => 'required',
            'category.exists' => 'invalid category data',
            'description.required' => 'required',
            'image.image' => 'must be a valid image'
        ];
    }
}
