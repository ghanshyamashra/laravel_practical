<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CustomerInquiryController@inquiryListing');
Route::get('customer-inquiry/create', 'CustomerInquiryController@create');
Route::get('customer-inquiry/{id}/edit', 'CustomerInquiryController@edit');
Route::get('customer-inquiry/{id}/delete', 'CustomerInquiryController@delete');
Route::post('customer-inquiry/save', 'CustomerInquiryController@save');

