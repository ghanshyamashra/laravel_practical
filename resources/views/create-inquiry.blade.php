@extends('welcome')

@section('container')

<div class="row">
    <form action="{{ url('customer-inquiry/save') }}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    Customer Name
                </label>
                <input type="text" name="name" class="form-control" value="{{ old('name')}}" placeholder="Enter Customer Name">
                @if($errors->has('name'))
                    <span class="text-danger">
                    {{$errors->first('name')}}
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    Customer Email
                </label>
                <input type="email" name="email" class="form-control" value="{{ old('email')}}" placeholder="inquiry@example.com">
                @if($errors->has('email'))
                    <span class="text-danger">
                    {{$errors->first('email')}}
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    Category
                </label>
                <select name="category"  class="form-control">
                    @foreach($categories as $c)
                        <option value="{{ $c->id }}" @if( old('category') == $c->id) selected @endif > {{ $c->name }}</option>
                    @endforeach
                </select>
                @if($errors->has('category'))
                    <span class="text-danger">
                    {{$errors->first('category')}}
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    Image
                </label>
                <input type="file" name="image">
                @if($errors->has('image'))
                    <span class="text-danger">
                    {{$errors->first('image')}}
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    Description
                </label>
                <textarea name="description"  class="form-control">{{old('description')}}</textarea>
                @if($errors->has('description'))
                    <span class="text-danger">
                    {{$errors->first('description')}}
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <button class="btn btn-info"> Save </button>
            <a href="{{ url('/')}}" class="btn btn-danger">
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection