@extends('welcome')

@section('container')

<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Customer Name
                    </th>
                    <th>
                        Customer Email
                    </th>
                    <th>
                        Inquiry Category
                    </th>
                    <th>
                        Image
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
            @foreach($inquiries as $inquiry)
                <tr>
                    <td>
                        {{ $inquiry->name}}
                    </td>
                    <td>
                        {{ $inquiry->email}}
                    </td>
                    <td>
                        {{ $inquiry->category->name}}
                    </td>
                    <td>
                        <img src="{{ url('storage/inquiry/'.$inquiry->image) }} " height=100 width=100 class="img">
                    </td>
                    <td>
                        {{ $inquiry->description}}
                    </td>
                    <td>
                        <a href="{{ url('customer-inquiry/'. $inquiry->id .'/edit') }}" class="btn btn-primary"> Edit</a>
                        <a href="{{ url('customer-inquiry/'. $inquiry->id .'/delete') }}" class="btn btn-danger"> Delete</a>
                    </td>
                </tr>
            @endforeach
            @if( $inquiries->count() == 0)
            <tr>
                <td>    
                    No record Found
                </td>
            </tr>
            @endif
            </tbody>
            <tfoot>
                {{ $inquiries->links()}}
            </tfoot>
        </table>
    </div>
</div>
@endsection